from flask import render_template, flash, redirect
from app import app


@app.route('/', methods=('GET',))
def index():
    return render_template("index.html",
                           title="Home",
                           )


@app.route('/listes', methods=('GET',))
def listes():
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id>/editer', methods=('GET',))
def editer_liste(id: int):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id_liste>/editer/description/<int:id_produit_liste>', methods=('GET',))
def description_produit_liste(id_liste: int, id_produit_liste):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id_liste>/editer/supprimer/<int:id_produit_liste>', methods=('GET',))
def supprimer_produit_liste(id_liste: int, id_produit_liste):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id_liste>/editer/modifier/<int:id_produit_liste>', methods=('GET',))
def modifier_produit_liste(id_liste: int, id_produit_liste):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id_liste>/editer/ajouter/<int:id_produit_liste>', methods=('GET',))
def ajouter_produit_liste(id_liste: int, id_produit_liste):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id>/supprimer', methods=('GET',))
def supprimer_liste(id: int):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id>/telecharger', methods=('GET',))
def telecharger_liste(id: int):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id>/fusionner', methods=('GET',))
def fusionner_liste(id: int):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id>/imprimer', methods=('GET',))
def imprimer_liste(id: int):
    return "Page non encore implémentée", 501


@app.route('/liste/creer', methods=('GET',))
def creer_liste():
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id>/calculer_prix/enseigne/<int:enseigne>', methods=('GET',))
def calculer_prix_enseigne_liste(id: int, enseigne: int):
    return "Page non encore implémentée", 501


@app.route('/liste/<int:id>/calculer_prix/maximum/<float:prix_max>', methods=('GET',))
def calculer_prix_max_liste(id: int, prix_max: float):
    return "Page non encore implémentée", 501


@app.route('/categories', methods=('GET',))
def categories():
    return "Page non encore implémentée", 501


@app.route('/magasins', methods=('GET',))
def magasins():
    return "Page non encore implémentée", 501


@app.route('/recettes', methods=('GET',))
def recettes():
    return "Page non encore implémentée", 501
